#!/bin/bash

############################################################################################
##CLEAN UP & QUALITY CHECK
############################################################################################
echo -e "\033[46m############################################################################################\nCLEAN UP & QUALITY CHECK\n############################################################################################\033[0m"

echo "--------------------------------------------------------------------------------"
echo "$d : Starting point to create first output folders"
echo "--------------------------------------------------------------------------------"

mkdir $path/PreQualCheck
mkdir $path/CleanUp
mkdir $path/CleanUp/RenamedFiles
mkdir $path/PostQualCheck

echo "--------------------------------------------------------------------------------"
echo "$d : Unzip zipped original row data files"
echo "--------------------------------------------------------------------------------"
###If the org. files are zipped, unzip the files here
cd $path/OrgFiles/
gunzip *.fastq.gz

N=$f
#(
for ((i=0; i<$f; i++)); do
###((i=i%f)); ((i++)) && wait
###open the parallel work
(   #echo "$i"
    ###Here the quality check with FastQC and read trimming with Trimmomatic will be performe.
    echo "$d : CleanUp for Sample: ${meinarrayNEW[$i]}"
    ###touch $path/CleanUp/readDist_${meinarrayNEW[$i]}.txt
    mkdir $path/PreQualCheck/Pre_${meinarrayNEW[$i]}

    echo "--------------------------------------------------------------------------------"
    echo "$d : Pre-FastQC scan: Of row reads: ${meinarrayNEW[$i]}"
    ###echo -e "\033[34mFastQC: a quality control tool for high throughput sequence data.Andrews S. (2010).\nVersion: 0.11.5\033[0m"
    echo "--------------------------------------------------------------------------------"
    $pathT/FastQC/./fastqc $path/OrgFiles/${meinarrayF[$i]}.fastq -o $path/PreQualCheck/Pre_${meinarrayNEW[$i]}/
    $pathT/FastQC/./fastqc $path/OrgFiles/${meinarrayR[$i]}.fastq -o $path/PreQualCheck/Pre_${meinarrayNEW[$i]}/

###close the parallel work
) &
    ###allow only to exetute $N jobs in parallel
    if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
        ###wait only for first job
        wait -n
    fi

done
wait

for ((i=0; i<$f; i++)); do
(
    echo "--------------------------------------------------------------------------------"
    echo "$d : Quality selection and row read trimming with Trimmomatic: ${meinarrayNEW[$i]}"
    ###echo -e "\033[34mTrimmomatic: A flexible trimmer for Illumina Sequence Data. Bioinformatics, btu170.Bolger, A. M., Lohse, M., & Usadel, B. (2014).\nVersion: 0.36\033[0m"
    echo "--------------------------------------------------------------------------------"
###sudo apt-get install parallel
        mkdir $path/CleanUp/Clean_${meinarrayNEW[$i]}
        mkdir $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}
        java -jar $pathT/Trimmomatic-0.36/./trimmomatic-0.36.jar PE -phred33 $path/OrgFiles/${meinarrayF[$i]}.fastq $path/OrgFiles/${meinarrayR[$i]}.fastq -baseout $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}.fastq LEADING:3 TRAILING:3 SLIDINGWINDOW:4:20 AVGQUAL:20 MINLEN:35 CROP:151 -threads 1 2>&1 | tee -a $path/CleanUp/trim_logfile.txt
        ### CROP:$CROP CROP:151 (maximal allowed read length)
) &

    if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
        wait -n
    fi

done
wait
        
for ((i=0; i<$f; i++)); do

        echo "--------------------------------------------------------------------------------"
        echo "$d : File header check"
        echo -e "\033[43m-- There are spaces inside the header of 1P, 2P, 1U or 2U before forward/reverse information (e.g. like Illumina based files), then option YES is default?: \033[0m"
            read -p "Check the header -- Yes: The space will be deleted. Not: The analyzing runs without header modification, because the forward/reverse information stands also on the front: (y/n)" response
            if [ "$response" == "y" ]
            then
###(
            cat $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P.fastq | sed -e 's/ //g' > $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P_temp.fastq
            cat $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P.fastq | sed -e 's/ //g' > $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P_temp.fastq
            cat $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U.fastq | sed -e 's/ //g' > $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U_temp.fastq
            cat $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U.fastq | sed -e 's/ //g' > $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U_temp.fastq
            rm $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P.fastq
            rm $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P.fastq
            rm $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U.fastq
            rm $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U.fastq
            mv $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P_temp.fastq $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P.fastq
            mv $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P_temp.fastq $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P.fastq
            mv $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U_temp.fastq $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U.fastq
            mv $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U_temp.fastq $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U.fastq

            ###For different steps different input files are required. 
            ###Therefor now files with only-interleaved-paired-end-reads, only-single-unpaired-reads and one total file with all reads are generated!
            echo "--------------------------------------------------------------------------------"
            echo "$d : Generate interleaved files (only PE) with a additional tool of the IDBA_UD assembler and other fastq files for post-qual-check: ${meinarrayNEW[$i]}"
            ###echo -e "\033[34mIDBA_UD: a de novo assembler for single-cell and metagenomic sequencing data with highly uneven depth. Peng, Y., et al. (2012). Bioinformatics, 28, 1420-1428\033[0m"
            echo "--------------------------------------------------------------------------------"
            cp $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}* $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/
            mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/forward_reads.fastq
            mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/reverse_reads.fastq

            perl $pathT/velvet_1.2.10/contrib/shuffleSequences_fasta/./shuffleSequences_fastq.pl $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/forward_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/reverse_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/output.fastq
            wait
            mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/output.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fastq
            cp $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fastq $path/CleanUp/

            $pathT/MaxBin-2.2.4/auxiliary/idba-1.1.1/bin/./fq2fa --merge --filter $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/forward_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/reverse_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fasta
            ###mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/output.fasta $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fasta
            cp $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fasta $path/CleanUp/

            echo "--------------------------------------------------------------------------------"
            echo "$d : Output file transformation: create different combined output variants (${meinarrayNEW[$i]})"
            echo -e "\033[33mCombinedUP*=all unpaired reads\nTotal*=CombinedUP+InterleavedPE(all reads)\nFind the output files under $path/CleanUp/\033[0m"
            echo "--------------------------------------------------------------------------------"
            cat $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U.fastq $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U.fastq > $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq

            cat $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq $path/CleanUp/InterleavedPE_${meinarrayNEW[$i]}.fastq > $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq

            echo "--------------------------------------------------------------------------------"
            echo "$d : Calculate the read length disturbance depending on trimmed reads (based on the Total_${meinarrayNEW[$i]}.fastq file)"
            echo -e "\033[33mLength vs. Amount will be illstrated on the readDist_${meinarrayNEW[$i]}.txt file\033[0m"
            echo "--------------------------------------------------------------------------------"
            awk 'NR%4==2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq 2>&1 | tee -a $path/CleanUp/readDist_clean_${meinarrayNEW[$i]}.txt


            mkdir $path/PostQualCheck/Post_${meinarrayNEW[$i]}
            ###Check the quality of all cleaned reads finally.
            echo "--------------------------------------------------------------------------------"
            echo "$d : Post-FastQC scan: Of trimmed reads: ${meinarrayNEW[$i]}"
            ###echo -e "\033[34mFastQC: a quality control tool for high throughput sequence data.Andrews S. (2010).\nVersion: 0.11.5\033[0m"
            echo "--------------------------------------------------------------------------------"
            $pathT/FastQC/./fastqc $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq -o $path/PostQualCheck/Post_${meinarrayNEW[$i]}/
            $pathT/FastQC/./fastqc $path/CleanUp/InterleavedPE_${meinarrayNEW[$i]}.fastq -o $path/PostQualCheck/Post_${meinarrayNEW[$i]}/
            $pathT/FastQC/./fastqc $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq -o $path/PostQualCheck/Post_${meinarrayNEW[$i]}/
            else
                    echo -e "\033[31mReady\033[0m"
            fi

###) &

### if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
###     wait -n
### fi
###wait

            if [ "$response" == "n" ]
                then
            ###For different steps different input files are required. 
            ###Therefor now files with only-interleaved-paired-end-reads, only-single-unpaired-reads and one total file with all reads are generated!
            echo "--------------------------------------------------------------------------------"
            echo "$d : Generate interleaved files (only PE) with a additional tool of the IDBA_UD assembler and other fastq files for post-qual-check: ${meinarrayNEW[$i]}"
            ###echo -e "\033[34mIDBA_UD: a de novo assembler for single-cell and metagenomic sequencing data with highly uneven depth. Peng, Y., et al. (2012). Bioinformatics, 28, 1420-1428\033[0m"
            echo "--------------------------------------------------------------------------------"
            cp $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}* $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/
            mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/forward_reads.fastq &
            mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/reverse_reads.fastq

            perl $pathT/velvet_1.2.10/contrib/shuffleSequences_fasta/./shuffleSequences_fastq.pl $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/forward_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/reverse_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/output.fastq
            mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/output.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fastq
            cp $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fastq $path/CleanUp/

            $pathT/MaxBin-2.2.4/auxiliary/idba-1.1.1/bin/./fq2fa --merge --filter $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/forward_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/reverse_reads.fastq $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fasta
            ###mv $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/output.fasta $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fasta
            cp $path/CleanUp/RenamedFiles/${meinarrayNEW[$i]}/InterleavedPE_${meinarrayNEW[$i]}.fasta $path/CleanUp/

            echo "--------------------------------------------------------------------------------"
            echo "$d : Output file transformation: create different combined output variants (${meinarrayNEW[$i]})"
            echo -e "\033[33mCombinedUP*=all unpaired reads\nTotal*=CombinedUP+InterleavedPE(all reads)\nFind the output files under $path/CleanUp/\033[0m"
            echo "--------------------------------------------------------------------------------"
            cat $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1U.fastq $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2U.fastq > $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq

            cat $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq $path/CleanUp/InterleavedPE_${meinarrayNEW[$i]}.fastq > $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq

            echo "--------------------------------------------------------------------------------"
            echo "$d : Calculate the read length disturbance depending on trimmed reads (based on the Total_${meinarrayNEW[$i]}.fastq file)"
            echo -e "\033[33mLength vs. Amount will be illstrated on the readDist_${meinarrayNEW[$i]}.txt file\033[0m"
            echo "--------------------------------------------------------------------------------"
            awk 'NR%4==2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq 2>&1 | tee -a $path/CleanUp/readDist_clean_${meinarrayNEW[$i]}.txt


            mkdir $path/PostQualCheck/Post_${meinarrayNEW[$i]}
            ###Check the quality of all cleaned reads finally.
            echo "--------------------------------------------------------------------------------"
            echo "$d : Post-FastQC scan: Of trimmed reads: ${meinarrayNEW[$i]}"
            ###echo -e "\033[34mFastQC: a quality control tool for high throughput sequence data.Andrews S. (2010).\nVersion: 0.11.5\033[0m"
            echo "--------------------------------------------------------------------------------"
            $pathT/FastQC/./fastqc $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq -o $path/PostQualCheck/Post_${meinarrayNEW[$i]}/
            $pathT/FastQC/./fastqc $path/CleanUp/InterleavedPE_${meinarrayNEW[$i]}.fastq -o $path/PostQualCheck/Post_${meinarrayNEW[$i]}/
            $pathT/FastQC/./fastqc $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq -o $path/PostQualCheck/Post_${meinarrayNEW[$i]}/
            else
                echo -e "\033[32mReady\033[0m"
            fi
done
###wait
echo "all done"

rm -r $path/CleanUp/RenamedFiles/
sleep 10s

