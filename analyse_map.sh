#!/bin/bash

############################################################################################
##PRESELECTION OF EXPECTED AND UNEXPECTED READS
############################################################################################
echo -e "\033[46m############################################################################################\nPRESELECTION OF EXPECTED AND UNEXPECTED READS\n############################################################################################\033[0m"

sleep 10s
###Input for Bowtie2 are the cleaned reads and reference genomes
###Please integrate all important reference genomes in the folder "Reference/BaseFasta"
echo "--------------------------------------------------------------------------------"
echo "$d : Create new output folders for Bowtie2"
###echo -e "\033[34mBOWTIE2: Fast gapped-read alignment with Bowtie 2. Nature Methods. 2012, 9:357-359.Langmead B, Salzberg S.\nVersion2.3.2\033[0m"
echo "--------------------------------------------------------------------------------"

mkdir $path/Bowtie2
mkdir $path/Bowtie2/IDX/

###In order to take all possible reference fasta files inside the bowtie index a pseudofile must be generated.
###This pseudofile obtained all files names in one line, selected with a comma.
###This line will be readed as input for the bowtie-build command.
echo "--------------------------------------------------------------------------------"
echo "$d : Prepare combined different reference file in FASTA to community_total.fasta, as base for bowtie2 "
echo "--------------------------------------------------------------------------------"
###cat $path/Reference/BaseFasta/*.fasta >> $path/Reference/community_total.fasta
cd $path/Reference/
rm $path/Reference/log_basefasta.txt
rm $path/Reference/log_combifasta_prep.txt
rm $path/Reference/log_combifasta.txt
cd $path/Reference/BaseFasta/
ls *.fasta > $path/Reference/log_basefasta.txt
wc -l $path/Reference/log_basefasta.txt
while read line; do
    echo -n "$line," >> $path/Reference/log_combifasta_prep.txt
    cat $path/Reference/log_combifasta_prep.txt | sed -e 's/,$//g' >  $path/Reference/log_combifasta.txt
    INPUT="$(cat $path/Reference/log_combifasta.txt)"
    echo "$INPUT"
### rm $path/Reference/log_combifasta_prep.txt
done < $path/Reference/log_basefasta.txt

###Use the prepared pseudofile.
###Generate bowtie index of all potential reference sequences.
echo "--------------------------------------------------------------------------------"
echo "$d : Create Bowtie2 index based on community_total.fasta"
echo "--------------------------------------------------------------------------------"
echo "$d : NOTE: Depending on the number of input communities, it may take a while"
cd $path/Reference/BaseFasta/
$pathT/bowtie2-2.3.2/./bowtie2-build $INPUT $path/Bowtie2/IDX/community_idx 2>&1 | tee -a $path/Bowtie2/idx_logfile.txt
$pathT/bowtie2-2.3.2/./bowtie2-inspect -s $path/Bowtie2/IDX/community_idx 2>&1 | tee -a $path/Bowtie2/idx_inspect_logfile.txt
sleep 20s
echo "--------------------------------------------------------------------------------"
echo "$d : Mapped the cleaned and trimmed reads based on your own reference index"
echo "--------------------------------------------------------------------------------"
echo "$d : NOTE: depending on the number of input reads, it may take a while"
###number of parallel jobs
N=$f
for ((i=0; i<$f; i++)); do
    echo "$d : Bowtie2 for Sample: ${meinarrayNEW[$i]}"
    mkdir $path/Bowtie2/${meinarrayNEW[$i]}
    mkdir $path/Bowtie2/${meinarrayNEW[$i]}/unconc
    mkdir $path/Bowtie2/${meinarrayNEW[$i]}/alconc
done
for ((i=0; i<$f; i++)); do
(
    $pathT/bowtie2-2.3.2/./bowtie2 --sensitive-local -a --fr -x $path/Bowtie2/IDX/community_idx -1 $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_1P.fastq -2 $path/CleanUp/Clean_${meinarrayNEW[$i]}/Clean_${meinarrayNEW[$i]}_2P.fastq -U $path/CleanUp/CombinedUP_${meinarrayNEW[$i]}.fastq -S $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sam -p 1 2>&1 | tee -a $path/Bowtie2/${meinarrayNEW[$i]}/bowtie_${meinarrayNEW[$i]}_logfile.txt
) &
    ###allow only to exetute $N jobs in parallel
    if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
        ###wait only for first job
        wait -n
    fi

done
wait
###for SE
### $pathT/bowtie2-2.3.2/./bowtie2 --sensitive-local --un $path/Bowtie2/${meinarrayNEW[$i]}/un/ --al $path/Bowtie2/${meinarrayNEW[$i]}/al/ -x $path/Bowtie2/IDX/community_idx -U $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq -S $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sam 2>&1 | tee -a $path/Bowtie2/${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_logfile.txt
###done

sleep 20s

##Than the generated SAM file must be transformed in a BAM file to generate a statistical file of mapped reads.
echo "--------------------------------------------------------------------------------"
echo "$d : Transformed the SAM (Bowtie2 output) to BAM"
##echo -e "\033[34mThe Sequence alignment/map (SAM) format and SAMtools. Bioinformatics, 25, 2078-9. Li H.*, Handsaker B.*, Wysoker A., Fennell T., Ruan J., Homer N., Marth G., Abecasis G., Durbin R. and 1000 Genome Project Data Processing Subgroup (2009) [PMID: 19505943]\033[0m"
echo "--------------------------------------------------------------------------------"
for ((i=0; i<$f; i++)); do
    echo "$d : Samtools for Sample: ${meinarrayNEW[$i]}"
    samtools view -bS $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sam > $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.bam
    samtools sort $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.bam -o $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sorted.bam
    ##alternative you can use bedtools bam2fastq
##  $pathT/BCL2BAM2FASTQ/bam2fastq/./bam2fastq $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sorted.bam $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie_sorted.fastq
##  $pathT/BCL2BAM2FASTQ/bam2fastq/./bam2fastq $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.bam $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.fastq
    ##this fastq file should be puted into DIAMOND look at the ReadME to generate the *.sub file for cluster usage
    samtools index $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sorted.bam
    samtools idxstats $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.sorted.bam > $path/Bowtie2/${meinarrayNEW[$i]}/idx_stats_${meinarrayNEW[$i]}
    ##The output is TAB-delimited with each line consisting of reference sequence name, sequence length, # mapped reads and # unmapped reads.
done

sleep 20s

###To use the BAM file usefull for further steps, it must be transformed in fastq/fasta files.
###Here same steps to sort, check and transforme the BAM file are defined.
###Thereby duplicated sequences (e.g. PCR-duplicates) will be deleted (only single PE reads are used)
echo "--------------------------------------------------------------------------------"
echo "$d : Prepared the unmapped files (unmapped files) and combined forward & reverse of unmapped reads to one total files."
echo "--------------------------------------------------------------------------------"

mkdir $path/Bowtie2/Unmappedfiles

for ((i=0; i<$f; i++)); do
        mkdir $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}
        echo "$d : Unmap_${meinarrayNEW[$i]}: Select unmapped paired end alignments (with read unmapped and mate unmapped)"
        samtools view -u -f 12 $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.bam > $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.unmapped.bam
####        echo "$d : Unmap_${meinarrayNEW[$i]}: SAM to BAM"
####        samtools view -bS $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/unmap_${meinarrayNEW[$i]}.unmapped.sam > $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/unmap_${meinarrayNEW[$i]}.unmapped.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: Sorted unmapped paired end the BAM file by name"
        samtools sort -n $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.unmapped.bam -o $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.sorted.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: Fixmate of unmapped paired end"
        samtools fixmate $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.sorted.bam $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.fixed.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: Sorted the fixed BAM file of unmapped paired end by name"
        samtools sort -n $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.fixed.bam -o $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.fixed.sorted.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: BAM to FastQ (unmapped paired end)"
        ###One combined file
        ###bamToFastq -i $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/unmap_${meinarrayNEW[$i]}.fixed.sorted.bam -fq $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/unmapped_${meinarrayNEW[$i]}_total.fastq
        ###Two selected files (FR)
        sleep 20s
        bamToFastq -i $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}.fixed.sorted.bam -fq $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_total.fastq
        ###echo "$d : Unmap_${meinarrayNEW[$i]}: Delete possible read duplicates with clumpify.sh by bbmap (unmapped paired end)"
        ###$pathT/bbmap/clumpify.sh in=$path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_total.fastq out=$path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_total_checked.fastq dedupe
        sleep 20s
        echo "$d : Unmap_${meinarrayNEW[$i]}: FastQ to FastA (unmapped paired end)"
        fastq_to_fasta -i $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_total.fastq -o $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmapped_${meinarrayNEW[$i]}_total.fasta &
        sleep 30s
        echo "--------------------------------------------------------------------------------"
        echo "$d : Calculate the read length disturbance depending on paired end unmapped reads (interleaved PE calc)"
        echo -e "\033[34mLength vs. Amount will be illstrated on the log_readlgth_${meinarrayNEW[$i]}_unmapped.txt file\033[0m"
        echo -e "\033[35mTo Calculate the k-mer, use the average read length minus 10 bp to define k on Main.sh (calc manual via Excel)[Source: Zerbino]\033[0m"
        echo "--------------------------------------------------------------------------------"
        awk 'NR%4==2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmapped_${meinarrayNEW[$i]}_total.fasta 2>&1 | tee -a $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/readlgth_${meinarrayNEW[$i]}_intPE_unmap.txt
        ###$pathT/MaxBin-2.2.3/auxiliary/idba-1.1.1/bin/./fq2fa --merge --filter $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_F.fastq $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_R.fastq $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmap_${meinarrayNEW[$i]}_total.fasta
                
        echo "$d : Unmap_${meinarrayNEW[$i]}: Select the total unmapped alignments"
        samtools view -u -f 4 $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.bam > $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.unmapped.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: Sorted total unmapped the BAM file by name"
        samtools sort -n $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.unmapped.bam -o $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.sorted.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: Fixmate of total unmapped"
        samtools fixmate $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.sorted.bam $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.fixed.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: Sorted the fixed BAM file of total unmapped by name"
        samtools sort -n $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.fixed.bam -o $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.fixed.sorted.bam
        echo "$d : Unmap_${meinarrayNEW[$i]}: BAM to FastQ (unmapped total)"
        ###For SE
        bamToFastq -i $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmap_${meinarrayNEW[$i]}.fixed.sorted.bam -fq $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total.fastq
        ###echo "$d : Unmap_${meinarrayNEW[$i]}: Delete possible read duplicates with clumpify.sh by bbmap"
        ###$pathT/bbmap/clumpify.sh in=$path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total.fastq out=$path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total_checked.fastq dedupe
        sleep 20s
        echo "$d : Unmap_${meinarrayNEW[$i]}: FastQ to FastA (unmapped total)"
        fastq_to_fasta -i $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total.fastq -o $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total.fasta
        sleep 20s
        echo "--------------------------------------------------------------------------------"
        echo "$d : Calculate the read length disturbance depending on unmapped reads (total unmapped reads)"
        echo -e "\033[33mLength vs. Amount will be illstrated on the log_readlgth_${meinarrayNEW[$i]}_unmapped.txt file.\033[0m"
        echo -e "\033[33mTo Calculate the k-mer, use the average read length minus 10 bp to define k on Main.sh (calc manual via Excel)[Source: Zerbino]\033[0m"
        echo "--------------------------------------------------------------------------------"
        awk 'NR%4==2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total.fasta 2>&1 | tee -a $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/readlgth_${meinarrayNEW[$i]}_all_unmap.txt
done

sleep 15s

echo "--------------------------------------------------------------------------------"
echo "$d : Prepared the mapped files (mapped files) and combined forward & reverse of mapped reads to one total files."
echo "--------------------------------------------------------------------------------"

mkdir $path/Bowtie2/Mappedfiles

for ((i=0; i<$f; i++)); do
        mkdir $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}
        echo "$d : Map_${meinarrayNEW[$i]}: Select total mapped alignments (without read unmapped and mate unmapped)"
        samtools view -u -F 2316 $path/Bowtie2/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_bowtie.bam > $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.mapped.bam
####        echo "$d : Map_${meinarrayNEW[$i]}: SAM to BAM"
####        samtools view -bS $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.mapped.sam > $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.mapped.bam
        echo "$d : Map_${meinarrayNEW[$i]}: Sorted the BAM file of total mapped by name"
        samtools sort -n $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.mapped.bam -o $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.sorted.bam
        echo "$d : Map_${meinarrayNEW[$i]}: Fixmate of total mapped"
        samtools fixmate $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.sorted.bam $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.fixed.bam
        echo "$d : Map_${meinarrayNEW[$i]}: Sorted the fixed BAM file of total mapped by name"
        samtools sort -n $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.fixed.bam -o $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.fixed.sorted.bam
        echo "$d : Map_${meinarrayNEW[$i]}: BAM to FastQ (all mapped reads)"
        ###One combined file
        bamToFastq -i $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.fixed.sorted.bam -fq $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_total.fastq
        ###echo "$d : Map_${meinarrayNEW[$i]}: Delete possible read duplicates with clumpify.sh by bbmap"
        ###$pathT/bbmap/clumpify.sh in=$path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_total.fastq out=$path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_total_checked.fastq dedupe
        ###Two selected files (FR)
        ###bamToFastq -i $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}.fixed.sorted.bam -fq $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_F.fastq -fq2 $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_R.fastq
        ###perl $pathT/velvet_1.2.10/contrib/shuffleSequences_fasta/./shuffleSequences_fastq.pl $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/forward_reads.fastq $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/reverse_reads.fastq $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/output.fastq
        
        echo "$d : Map_${meinarrayNEW[$i]}: FastQ to FastA (all mapped reads)"
        fastq_to_fasta -i $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_total.fastq -o $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/all_mapped_${meinarrayNEW[$i]}_total.fasta

        
        ###$pathT/MaxBin-2.2.3/auxiliary/idba-1.1.1/bin/./fq2fa --merge --filter $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_F.fastq $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/map_${meinarrayNEW[$i]}_R.fastq $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/mapped_${meinarrayNEW[$i]}_total.fasta
        ###echo "$d : Map_${meinarrayNEW[$i]}: Format check of total mapped file --> generate the final mapped_total file"
        ###$pathSH/additionalSH/check_builded_fastq_script < $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/mapped_${meinarrayNEW[$i]}_total_unchecked.fastq > $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/mapped_${meinarrayNEW[$i]}_total.fastq
        
        echo "--------------------------------------------------------------------------------"
        echo "$d : Calculate the read length disturbance depending on mapped reads (all mapped reads)"
        echo -e "Length vs. Amount will be illstrated on the log_readlgth_${meinarrayNEW[$i]}_mapped.txt file."
        echo -e "To Calculate the k-mer, use the average read length minus 10 bp to define kdef on Main.sh (calc manual via Excel)[Source: Zerbino]"
        echo "--------------------------------------------------------------------------------"
        awk 'NR%4==2 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/all_mapped_${meinarrayNEW[$i]}_total.fasta 2>&1 | tee -a $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/log_all_readlgth_${meinarrayNEW[$i]}_mapped.txt
        echo -e "\033[33mNOTE: these fasta files of mapped reads should be puted directly into DIAMOND. Look at the ReadME to generate the *.sub file for cluster usage\033[0m."
done

sleep 15s
