#!/bin/bash

############################################################################################
#TRANSFORMATION ZONE
############################################################################################

###Define all important path
###Path to the desired main folder to store you output (results)
###E.g: path=/home/user/Exp_ID
path=/media/administrator/Elements_FB1/MCB-MG-Pipeline/Validierung/Mock_Pipetest_3sample

###NOTE: If the tool on a fix harddisc, oben this disc before or save the tool on your $PATH
###Path to the desired tool folder (all installed tools in one folder)
###E.g: pathT=/home/user/Tools
pathT=/media/administrator/DA1A2D701A2D4B39/PipelineTools
##Path to folder that contains the executable *.sh files
###E.g: pathSH=/home/user/MCB-MGPipeline/bin
pathSH=/media/administrator/Elements_FB1/MCB-MG-Pipeline/bin

###SampleIDS
###NOTE: save the original file under $path/OrgFiles
###NOTE: rename files firstly (clear sample depending name Org_SampleID_F.fastq.gz and Org_SampleID_R.fastq.gz)
##Number of Samples (Default: 3)
f=3
###Write all samples of one experiment (defaul: max three sample for one run) -- ATTENTION : all arrays starts on position 0!
meinarrayF=(Org_Sample1_F Org_Sample2_F Org_Sample3_F)
meinarrayR=(Org_Sample1_R Org_Sample2_R Org_Sample3_R)
meinarrayNEW=(Sample1 Sample2 Sample3)

###Define the name of experiment
###E.g: Exp=Experiment_ID, in this case ID could be a consecutive number, date or other samples described names
Exp=Experiment_TriMock
###Estimated number of bins (e.g. estimated number of unknown organism)
###b=3



############################################################################################
#ATTENTION#
#DON'T TOUCH THIS EXECUTABLE CODE FOR ANALYZING
############################################################################################
d=$(date +%y-%m-%d_%H:%M:%S)

###Generate default input and reference folders, depending on the following requests
mkdir $path/OrgFiles
mkdir $path/Reference
mkdir $path/Reference/BaseFasta

echo -e "\033[43mDecide to use the manual or automatically version\033[0m"
echo -e "\033[43m--manual: interposed questions about the completeness of main.sh settings, rename original files and the availabilty of reference fasta files under Reference/BaseFasta/\n--automatically: no interposed questions, all steps will be performed one by one\033[0m"
echo -e "\n"
echo -e "\033[5mNOTE:Please save the path to pplacer inside your path. If not CheckM will failed\033[0m"
echo -e "\n"
echo "--------------------------------------------------------------------------------"
			read -p "Do you like to run the manual mode? (y/n)" response
			if [ "$response" == "y" ]
			then
					###First request --> Transform and define important parameters and paths.
					echo -e "\033[43m-- Please change the path and arrays in main.sh (path|pathT|pathSH + meinarrayF|meinarrayR|meinarrayNEW)!\n-- Define the future experimental name!\033[0m"
					###echo "After 10 minutes the programm will be continued automatically without response!!"
					echo -e "\033[43mREADY (y/n)?: \033[0m"
							while ((!gueltig1)); do
							  ###read -sn1 -t 600 answer1
							  read -sn1 answer1
							  case "$answer1" in
								[JjYy])    result1=1; gueltig1=1 ;;
								[Nn])      result1=0; gueltig1=1 ;;
								"")        result1=1; gueltig1=1 ;;
								*)         gueltig1=0;
							  esac
							done
							echo
							if ((result1)); then
							  echo -e "Nice Job again!"
									###Second request --> Put the original row reads within the folder "OrgFiles".
									###Then the first analyzing part will be started (clean up) and after clean up a pre taxonomic check will be started
									echo -e "\033[43m-- Please store the original row read files inside 'OrgFiles'!\n-- RENAME THE FILES: Org_SampleID_F.fastq.gz and Org_SampleID_R.fastq.gz\033[0m"
									###echo "After 15 minutes the programm will be continued automatically without response!!"
									echo -e "\033[43mREADY (y/n)?: \033[0m"
										while ((!gueltig2)); do
										  ###read -sn1 -t 900 answer2
										  read -sn1 answer2
										  case "$answer2" in
											[JjYy])    result2=1; gueltig2=1 ;;
											[Nn])      result2=0; gueltig2=1 ;;
											"")        result1=1; gueltig1=1 ;;
											*)         gueltig2=0 ;;
										  esac
										done
										echo
										if ((result2)); then
										  echo -e "Nice Job again!"
					########################################################################################
															cd $pathSH/
															source $pathSH/cleanup.sh 2>&1 | tee -a $path/log_total_CleanUP.txt
															###Here the taxonomic check ist starting
															###It gives a general overview about possible organism and mainly it defines your individual DB for the bowtie mapping.
															echo "--------------------------------------------------------------------------------"
															echo "$d : Pre-Taxonomic Characterisation of total trimmed reads by Metaxa2"
															###echo -e "\033[34mMetaxa2: Improved Identification and Taxonomic Classification of Small and Large Subunit rRNA in Metagenomic Data. Molecular Ecology Resources (2015).Bengtsson-Palme J, Hartmann M, Eriksson KM, Pal C, Thorell K, Larsson DGJ, Nilsson RH\033[0m"
															echo -e "Look at the README file to find out, how you can transform the output to MetaPhlAn and to illustrate the output as Headmap"
															###echo -e "\033[34m'MetahPhlAn: Metagenomic microbial community profiling using unique clade-specific marker genes' Nicola Segata, Levi Waldron, Annalisa Ballarini, Vagheesh Narasimhan, Olivier Jousson, Curtis Huttenhower. Nature Methods, 8, 811–814, 2012\033[0m"
															echo "--------------------------------------------------------------------------------"
															mkdir $path/PreTax
															for ((i=0; i<$f; i++)); do
															echo "$d : Pre-Taxonomic Characterisation: ${meinarrayNEW[$i]}"
																mkdir $path/PreTax/${meinarrayNEW[$i]}
																$pathT/Metaxa2_2.1.3/metaxa2 -i $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq -o $path/PreTax/${meinarrayNEW[$i]}/pretax_${meinarrayNEW[$i]} -g SSU -q 20 -t bacteria,archaea 2>&1 | tee -a $path/PreTax/${meinarrayNEW[$i]}/log_m2_${meinarrayNEW[$i]}
																$pathT/Metaxa2_2.1.3/metaxa2_ttt -i $path/PreTax/${meinarrayNEW[$i]}/pretax_${meinarrayNEW[$i]}.taxonomy.txt -o $path/PreTax/${meinarrayNEW[$i]}/ttt_pretax_${meinarrayNEW[$i]} -t bacteria,archaea --summary T --lists T --separate T --unknown T 2>&1 | tee -a $path/PreTax/${meinarrayNEW[$i]}/log_ttt_${meinarrayNEW[$i]}
															done
															###If you have checked the Metaxa-output download and store the fasta file for your small DB inside the folder "Reference/BaseFasta".
															###These reference genome files must be in FASTA.
															echo -e "\033[43m-- Please store the reference genomes in FASTA (of your desired database) inside 'BaseFasta' under Reference!\033[0m"
															###echo "After 45 minutes the programm will be continued automatically without response!!"
															echo -e "\033[43mREADY (y/n)?: \033[0m"
															while ((!gueltig3)); do
															  ###read -sn1 -t 2700 answer3
															  read -sn1 answer3
															  case "$answer3" in
																[JjYy])    result3=1; gueltig3=1 ;;
																[Nn])      result3=0; gueltig3=1 ;;
																"")        result1=1; gueltig1=1 ;;
																*)         gueltig3=0 ;;
															  esac
															done
															echo
															if ((result3)); then
															  echo -e "Nice Job again!"
															###After checking all fundamental inputs the mapping and denovo part will be performed automatically
															cd $pathSH/
															source $pathSH/analyse_map.sh 2>&1 | tee -a $path/log_total_Map.txt
															cd $pathSH/
															source $pathSH/analyse_denovo.sh 2>&1 | tee -a $path/log_total_DeNovo.txt
					########################################################################################
							else
							  echo -e "\033[43mPlease response all questions with yes, otherwise the Main-script is not working\033[0m"
							fi
					###					else
					###		  			  echo -e "\033[43mPlease response all questions with yes, otherwise the Main-script is not working\033[0m"
										fi
					###								else
					###		  						  echo -e "\033[43mPlease response all questions with yes, otherwise the Main-script is not working\033[0m"
													fi
			fi
			if [ "$response" == "n" ]
				then
					read -p "Do you like to run the automatical mode? (y/n)" response
					if [ "$response" == "y" ]
					then
						echo -e "\033[43m--All settings in the Main.sh are changed\033[0m"
						echo -e "\033[43m--The original row read files are renamed depending the following pattern: Org_SampleID_F and Org_SampleID_R\033[0m"
						echo -e "\033[43m--All necessary and important reference FASTA files are stored under /Reference/BaseFasta/\033[0m"
							cd $pathSH/
							source $pathSH/cleanup.sh 2>&1 | tee -a $path/log_total_CleanUP.txt
							mkdir $path/PreTax
							for ((i=0; i<$f; i++)); do
								echo "$d : Pre-Taxonomic Characterisation: ${meinarrayNEW[$i]}"
								mkdir $path/PreTax/${meinarrayNEW[$i]}
								$pathT/Metaxa2_2.1.3/metaxa2 -i $path/CleanUp/Total_${meinarrayNEW[$i]}.fastq -o $path/PreTax/${meinarrayNEW[$i]}/pretax_${meinarrayNEW[$i]} -g SSU -q 20 -t bacteria,archaea 2>&1 | tee -a $path/PreTax/${meinarrayNEW[$i]}/log_m2_${meinarrayNEW[$i]}
								$pathT/Metaxa2_2.1.3/metaxa2_ttt -i $path/PreTax/${meinarrayNEW[$i]}/pretax_${meinarrayNEW[$i]}.taxonomy.txt -o $path/PreTax/${meinarrayNEW[$i]}/ttt_pretax_${meinarrayNEW[$i]} -t bacteria,archaea --summary T --lists T --separate T --unknown T 2>&1 | tee -a $path/PreTax/${meinarrayNEW[$i]}/log_ttt_${meinarrayNEW[$i]}
							done
							cd $pathSH/
							source $pathSH/analyse_map.sh 2>&1 | tee -a $path/log_total_Map.txt
							cd $pathSH/
							source $pathSH/analyse_denovo.sh 2>&1 | tee -a $path/log_total_DeNovo.txt
					fi
					if [ "$response" == "n" ]
						then
						echo -e "\033[43m--If some setting are missing, then the programm will failed and stopped early\033[0m"
						exit
					fi
				fi
echo -e "\033[46m############################################################################################\nFOLDER-CLEANER\n############################################################################################\033[0m"

###delete files, which are unnecessary 
###unnecessary intermediates files are deleted + txt files without content for analyzing + bam/sam files during sorting steps, etc.
###additional some folder will be zipped

#read -p "Do you like to delete intermediated and additional files (only important files for analyzing remained)? (y/n)" response
#			if [ "$response" == "y" ]
#			then
#				###CleanUP-Folder
#				for ((i=0; i<$f; i++)); do
#					echo -e "\033[43m : Compressed all cleaned read files\033[0m"
#					###compress cleaned files after analyse
#					tar cfvz $path/CleanUp/Clean_${meinarrayNEW[$i]}.tar.gz $path/CleanUp/Clean_${meinarrayNEW[$i]}/*.fastq
#					echo " : Compressed all original read files"
#					###Gzip Org_files
#					gzip $path/OrgFiles/Org_${meinarrayNEW[$i]}_F.fastq
#					gzip $path/OrgFiles/Org_${meinarrayNEW[$i]}_R.fastq
#					sleep 10s
#					rm $path/CleanUp/Clean_${meinarrayNEW[$i]}/*.fastq
#					rm -r $path/CleanUp/Clean_${meinarrayNEW[$i]}
#					rm $path/OrgFiles/Org_${meinarrayNEW[$i]}_F.fastq
#					rm $path/OrgFiles/Org_${meinarrayNEW[$i]}_R.fastq
#	
#					echo -e "\033[43m : Delete intermediated bam-files during the process of mapped vs. unmapped selection and compressed all necessary files\033[0m"
#					cd $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/
#					rm *.fixed.bam
#					rm *.fixed.sorted.bam
#					rm *.sorted.bam
#					tar cfvz $path/Bowtie2/Mappedfiles/Bowtie2_map_${meinarrayNEW[$i]}.tar.gz $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/
#					tar cfvz $path/Bowtie2/Bowtie2_${meinarrayNEW[$i]}.tar.gz $path/Bowtie2/${meinarrayNEW[$i]}/
#					sleep 10s
#					rm -r $path/Bowtie2/${meinarrayNEW[$i]}
#					rm -r $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}

#					cd $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/
#					rm *.fixed.bam
#					rm *.fixed.sorted.bam
#					rm *.sorted.bam
#					tar cfvz $path/Bowtie2/Unmappedfiles/Bowtie2_unmap_${meinarrayNEW[$i]}.tar.gz $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/
#					sleep 10s
#					rm -r $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}

#					echo -e "\033[43m : Delete intermediated assembly-files and compressed all necessary contig files\033[0m"
#					cd $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[$i]}/
#					rm align-*
#					rm contig-*.fa
#					rm graph-*
#					rm local-contig-*.fa
#					tar cfvz $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas.tar.gz $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/*
#					sleep 10s
#					rm -r $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/*

#					echo -e "\033[42mThe analyzing is finished successfull and only necessary files are saved\033[0m"
#				done
#			fi
#			if [ "$response" == "n" ]
#						then
#						echo -e "\033[42mThe analyzing is finished successfull and all intermediated files are saved\033[0m"
#			fi

