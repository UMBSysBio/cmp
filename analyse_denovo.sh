#!/bin/bash

###########################################################################################
#DE NOVO ANALYZING OF UNMAPPED (UNKNOWN) READS
###########################################################################################
echo -e "\033[46m############################################################################################\nDE NOVO ANALYZING OF UNMAPPED (UNKNOWN) READS\n############################################################################################\033[0m"

echo "--------------------------------------------------------------------------------"
echo "$d : Starts to assemble the unaligned (unmapped) reads with IDBA_UD"
###echo -e "\033[34mABySS: A parallel assembler for short read sequence data. Simpson JT, Wong K, Jackman SD, Schein JE, Jones SJ, Birol I. Genome Research, 2009-June. (Genome Research, PubMed)\nVersion:2.0.2\033[0m"
echo "--------------------------------------------------------------------------------"

mkdir $path/DeNovo
mkdir $path/DeNovo/Assembly
mkdir $path/DeNovo/Bin_Contig
mkdir $path/DeNovo/Bin_Contig/CheckM
mkdir $path/DeNovo/REAssembly
mkdir $path/DeNovo/Assembly/IDBA
mkdir $path/DeNovo/Assembly/Quast

for ((i=0; i<$f; i++)); do
    ###Start the assembly process with the IDBA_UD tool
### mkdir $path/DeNovo/Assembly/ABySS/abyss_${meinarrayNEW[$i]}
    mkdir $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[$i]}
### echo "--------------------------------------------------------------------------------"
### echo "$d : ABySS for Sample: ${meinarrayNEW[$i]}"
### echo "--------------------------------------------------------------------------------"
### ###select the optimal k-mer value based on k-mer check or calc based on average read length minus 10 bp
### abyss-pe name=$path/DeNovo/Assembly/ABySS/abyss_${meinarrayNEW[$i]}/${meinarrayNEW[$i]} k=$kdef in=$path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/all_unmapped_${meinarrayNEW[$i]}_total.fasta 2>&1 | tee $path/DeNovo/Assembly/ABySS/abyss_${meinarrayNEW[$i]}/log_abyss_${meinarrayNEW[$i]}.txt
done

###number of parallel jobs
N=$f
for ((i=0; i<$f; i++)); do
(
    echo "--------------------------------------------------------------------------------"
    echo "$d : IDBA_UD for Sample: ${meinarrayNEW[$i]}"
    echo "--------------------------------------------------------------------------------"

    $pathT/MaxBin-2.2.4/auxiliary/idba-1.1.1/bin/./idba_ud -r $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmapped_${meinarrayNEW[$i]}_total.fasta -o $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[$i]} --mink 51 --maxk 151 --step 10 --min_contig 1000 --pre_correction --num_threads 1 2>&1 | tee $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[$i]}/logfile_ibda_${meinarrayNEW[$i]}.txt
    ###abyss-pe name=$path/DeNovo/Assembly/${meinarrayNEW[$i]}/${meinarrayNEW[$i]} k=$kdef in=$path/DeNovo/Unmappedfiles/${meinarrayNEW[$i]}/unmapped_${meinarrayNEW[$i]}_F.fastq $path/DeNovo/Unmappedfiles/${meinarrayNEW[$i]}/unmapped_${meinarrayNEW[$i]}_R.fastq 2>&1 | tee $path/DeNovo/Assembly/${meinarrayNEW[$i]}/log_abyss_${meinarrayNEW[$i]}.txt
) &
    ###allow only to exetute $N jobs in parallel
    if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
        ###wait only for first job
        wait -n
    fi

done
wait

sleep 20s


###Here the assembly Results will be compared
echo "--------------------------------------------------------------------------------"
echo "$d : Statistical analyze of ABySS output (created a short summary of assembly) with QUAST"
###echo -e "\033[34mQUAST: Alexey Gurevich, Vladislav Saveliev, Nikolay Vyahhi and Glenn Tesler,QUAST: quality assessment tool for genome assemblies,Bioinformatics (2013) 29 (8): 1072-1075.\033[0m"
echo "--------------------------------------------------------------------------------"
###NOTE: Please change the number of examples depending of the number of input samples (in this case: 3)
###python $pathT/quast-4.5/./quast.py $path/DeNovo/Assembly/${meinarrayNEW[0]}/${meinarrayNEW[0]}-6.fa -o $path/DeNovo/Quast/$Exp
python $pathT/quast-4.5/./quast.py $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[0]}/contig.fa $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[1]}/contig.fa $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[2]}/contig.fa -o $path/DeNovo/Assembly/Quast/idba_$Exp

###INPUT="$(cat $path/Reference/log_combifasta.txt)"
###mkdir $path/DeNovo/Assembly/Quast/metaquast
###python $pathT/quast-4.5/./metaquast.py -R $Input -o $path/DeNovo/Assembly/Quast/metaquast/ $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[0]}/contig.fa $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[1]}/contig.fa $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[2]}/contig.fa



###Here the clustering depending on MaxBin-algorithm will be performed
for ((i=0; i<$f; i++)); do
    echo "$d : Binning for Sample: ${meinarrayNEW[$i]}"
    mkdir $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}
done


for ((i=0; i<$f; i++)); do
###(
    echo "--------------------------------------------------------------------------------"
    echo "$d : Binning of contigs (based on IDBA_UD) with MaxBin2"
    ###echo -e "\033[34mMaxBin2: Wu YW, Tang YH, Tringe SG, Simmons BA, and Singer SW, 'MaxBin: an automated binning method to recover individual genomes from metagenomes using an expectation-maximization algorithm', Microbiome, 2:26, 2014.\033[0m"
    echo "--------------------------------------------------------------------------------"
    $pathT/MaxBin-2.2.4/./run_MaxBin.pl -contig $path/DeNovo/Assembly/IDBA/idba_${meinarrayNEW[$i]}/contig.fa -reads $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmapped_${meinarrayNEW[$i]}_total.fasta -reassembly -prob_threshold 0.8 -plotmarker -markerset 40 -out $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]} -thread 1 2>&1 | tee $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/maxbin_log_${meinarrayNEW[$i]}.txt
    ###marker set 40 is recommended by MaxBin for archaea and bacteria communities
###) &
### ###allow only to exetute $N jobs in parallel
### if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
###     ###wait only for first job
###     wait -n
### fi

done
###wait

for ((i=0; i<$f; i++)); do
###Check the successfully binning process depending on completeness and contamination rate with CheckM
###If CheckM failed, maybe export PATH=$PATH:/home/administrator/pplacer-Linux-v1.1.alpha19 the path to pplacer isn't in your path
    mkdir $path/DeNovo/Bin_Contig/CheckM/${meinarrayNEW[$i]}
    echo "--------------------------------------------------------------------------------"
    echo "$d : Check the completeness and quality of generated bins with CheckM"
    ###echo -e "\033[34mCheckM: Parks DH, Imelfort M, Skennerton CT, Hugenholtz P, Tyson GW. 2014. Assessing the quality of microbial genomes recovered from isolates, single cells, and metagenomes. Genome Research, 25: 1043-1055.\033[0m"
    echo "--------------------------------------------------------------------------------"
    rm -r $path/DeNovo/Bin_Contig/CheckM/${meinarrayNEW[$i]}/
    checkm lineage_wf -r -x fasta --nt -t 1 --reduced_tree --pplacer_threads 1 $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/ $path/DeNovo/Bin_Contig/CheckM/${meinarrayNEW[$i]} >> $path/DeNovo/Bin_Contig/CheckM/logfile_checkm_${meinarrayNEW[$i]}
    ###checkm bin_compare -x fasta -y fa $path/DeNovo/Assembly/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}-6.fa $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/ $path/DeNovo/Bin_Contig/CheckM/compare${meinarrayNEW[$i]}
    ###checkm qa -o 1 -f $path/DeNovo/Bin_Contig/CheckM/${meinarrayNEW[$i]} -t 1 $path/DeNovo/Bin_Contig/CheckM/${meinarrayNEW[$i]}/lineage.ms $path/DeNovo/Bin_Contig/CheckM/${meinarrayNEW[$i]}/
    ###-r -x fasta --nt -t 1 --pplacer_threads 1

                w="$(find $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem -type f -name "Bin_${meinarrayNEW[$i]}.reads.0*" | wc -w)"
                sleep 1s
                echo "Sample_${meinarrayNEW[$i]} had $w bins"
done
###wait

sleep 20s


for ((i=0; i<$f; i++)); do
        mkdir $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas
        ###Prepare the possible input for the external annotation with Diamond/MEGAN6
        mkdir $path/Annotation
        mkdir $path/Annotation/${meinarrayNEW[$i]}
        mkdir $path/Annotation/${meinarrayNEW[$i]}/mapped_annotation
        mkdir $path/Annotation/${meinarrayNEW[$i]}/mapped_annotation/input
        mkdir $path/Annotation/${meinarrayNEW[$i]}/mapped_annotation/output
        mkdir $path/Annotation/${meinarrayNEW[$i]}/unmapped_annotation
        mkdir $path/Annotation/${meinarrayNEW[$i]}/unmapped_annotation/input
        mkdir $path/Annotation/${meinarrayNEW[$i]}/unmapped_annotation/output
        
        w="$(find $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem -type f -name "Bin_${meinarrayNEW[$i]}.reads.0*" | wc -w)"
        sleep 2s

        echo "$d : Reassembly for Sample: ${meinarrayNEW[$i]}"
        echo "--------------------------------------------------------------------------------"
        echo "$d : Reassemble the generated bins via IDBA_UD (integrated in MaxBin2) to obtain (maybe) full reconstructed genomes (it is a experimental step)"
        ###echo -e "\033[34mIDBA_UD: Peng, Y., et al. (2012) IDBA-UD: a de novo assembler for single-cell and metagenomic sequencing data with highly uneven depth, Bioinformatics, 28, 1420-1428.\033[0m"
        ###Start the reassembly of binned/clustered reads.
        ###Thereby two different ways are possible.
        ###1) If bin.reads inside the reassembly folder are obtained --> use the clustered reads per bin files
        ###2) A reassembly was not possible --> than use the unclassified file of reads as one cluster of unmapped and unclassified reads (mainly for mockdata)
        echo "--------------------------------------------------------------------------------"
        echo "$d : IDBA_UD Reassembly: ${meinarrayNEW[$i]}"
        echo -e "\033[43m-- Is the contig file clustered and reassemled successfully within $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem/Bin_${meinarrayNEW[$i]}.reads.0*?: \033[0m"
            read -p "Bin_${meinarrayNEW[$i]}.reads.0* available (y/n)" response
            if [ "$response" == "y" ]
            then
###         if [ "$answer" = y -o "$answer" = Y ] ; then
###         if [ "'-e $reasfile'" ]; then
                    echo -e "\033[42mReassembly was successfully, folder contained reassemled read file (*.0X). Take the reassembled reads\033[0m"
                    ###for ((j=1; j<$b; j++)); do
                echo "Bin_${meinarrayNEW[$i]}.reassem contains $w bins"
                    for ((j=1; j<=$w; j++)); do
                        echo "$d : IDBA_UD Reassembly: ${meinarrayNEW[$i]} for the 0$j Bin"
                        mkdir $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin
                        $pathT/MaxBin-2.2.4/auxiliary/idba-1.1.1/bin/./idba_ud -r $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem/Bin_${meinarrayNEW[$i]}.reads.00$j -o $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_$j --mink 51 --maxk 151 --step 10 --min_contig 1000 --pre_correction --num_threads 2 2>&1 | tee $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/logfile_RA_${meinarrayNEW[$i]}
                        $pathT/MaxBin-2.2.4/auxiliary/idba-1.1.1/bin/./idba_ud -r $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem/Bin_${meinarrayNEW[$i]}.reads.0$j -o $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_$j --mink 51 --maxk 151 --step 10 --min_contig 1000 --pre_correction --num_threads 2 2>&1 | tee $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/logfile_RA_${meinarrayNEW[$i]}
                        cd $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_$j/
                        ###to avoid duplication after merge together of different idba_ud based contigs files, here the header will be transformed depending on sample-Id and Bin-number
                        sed s/\>contig/\>contig_${meinarrayNEW[$i]}_$j/g contig.fa > contig_${meinarrayNEW[$i]}_bin_$j.fa
                        ###sed s/\>contig/\>contig_${meinarrayNEW[$i]}_0$j/g contig.fa > contig_${meinarrayNEW[$i]}_bin_0$j.fa
                        cp $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_$j/contig_${meinarrayNEW[$i]}_bin_$j.fa $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin
                    done
                    cat $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin/*.fa > $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin/unm_${meinarrayNEW[$i]}_RAContig_total.fasta
                else
                    echo -e "\033[31mSwitch to unclass file\033[0m"
            fi
            if [ "$response" == "n" ]
                then
                    cp $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reads.noclass $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem/
                    mkdir $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin
                    echo -e "\033[41mReassembly failed, folder empty. Take the simple binned reads\033[0m"
                    echo -e "\033[31mUse alternative: Bin_${meinarrayNEW[$i]}.reads.noclass\033[0m"
                    $pathT/MaxBin-2.2.4/auxiliary/idba-1.1.1/bin/./idba_ud -r $path/DeNovo/Bin_Contig/${meinarrayNEW[$i]}/Bin_${meinarrayNEW[$i]}.reassem/Bin_${meinarrayNEW[$i]}.reads.noclass -o $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_noclass --mink 51 --maxk 151 --step 10 --min_contig 1000 --pre_correction --num_threads 2 2>&1 | tee $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/logfile_RA_${meinarrayNEW[$i]}
                    cd $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_noclass/
                    sed s/\>contig/\>contig_${meinarrayNEW[$i]}_unclass/g contig.fa > contig_${meinarrayNEW[$i]}_bin_unclass.fa
                    cat $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/${meinarrayNEW[$i]}_idba_noclass/contig_${meinarrayNEW[$i]}_bin_unclass.fa > $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin/unm_${meinarrayNEW[$i]}_RAContig_total.fasta
            else
                echo -e "\033[32mPer bin files were used\033[0m"
            fi
        cp $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin/unm_${meinarrayNEW[$i]}_RAContig_total.fasta $path/Annotation/${meinarrayNEW[$i]}/unmapped_annotation/input/
        cp $path/Bowtie2/Mappedfiles/map_${meinarrayNEW[$i]}/all_mapped_${meinarrayNEW[$i]}_total.fasta $path/Annotation/${meinarrayNEW[$i]}/mapped_annotation/input/
done
###wait

mkdir $path/DeNovo/REAssembly/PostRA_QUAST
###Here the REassembly results will be compared
###for ((i=0; i<$f; i++)); do 
echo "--------------------------------------------------------------------------------"
echo "$d : Statistical analyze of Reassembled output (created a short summary of assembly) with QUAST"
###echo -e "\033[34mQUAST: Alexey Gurevich, Vladislav Saveliev, Nikolay Vyahhi and Glenn Tesler,QUAST: quality assessment tool for genome assemblies,Bioinformatics (2013) 29 (8): 1072-1075.\033[0m"
echo "--------------------------------------------------------------------------------"
###NOTE: Please change the number of examples depending of the number of input samples (in this case: 3)
python $pathT/quast-4.5/./quast.py $path/DeNovo/REAssembly/${meinarrayNEW[0]}_reas/RAContigs_perBin/unm_${meinarrayNEW[0]}_RAContig_total.fasta $path/DeNovo/REAssembly/${meinarrayNEW[1]}_reas/RAContigs_perBin/unm_${meinarrayNEW[1]}_RAContig_total.fasta $path/DeNovo/REAssembly/${meinarrayNEW[2]}_reas/RAContigs_perBin/unm_${meinarrayNEW[2]}_RAContig_total.fasta -o $path/DeNovo/REAssembly/PostRA_QUAST/idba_RA_$Exp

#INPUT="$(cat $path/Reference/log_combifasta.txt)"
#mkdir $path/DeNovo/Assembly/Quast/metaquast
#python $pathT/quast-4.5/./metaquast.py -R $Input -o $path/DeNovo/Assembly/Quast/metaquast/ path/DeNovo/REAssembly/${meinarrayNEW[0]}_reas/RAContigs_perBin/unm_${meinarrayNEW[0]}_RAContig_total.fasta $path/DeNovo/REAssembly/${meinarrayNEW[1]}_reas/RAContigs_perBin/unm_${meinarrayNEW[1]}_RAContig_total.fasta $path/DeNovo/REAssembly/${meinarrayNEW[2]}_reas/RAContigs_perBin/unm_${meinarrayNEW[2]}_RAContig_total.fasta
###done

sleep 20s

echo "--------------------------------------------------------------------------------"
echo "$d : Map unmapped reads back to assembly (contigs) to calculate finally the potential community depending on read matches"
echo "--------------------------------------------------------------------------------"
######Map reads back to contigs######
###Find out which reads are included inside the contig###
###count the number of reads per contig###
###Find out which contig encoded which taxa###
###weighting: use the number of reads per contig per taxa als weighting###
###useful source: http://2014-5-metagenomics-workshop.readthedocs.io/en/latest/assembly/map.html ###

for ((i=0; i<$f; i++)); do
    mkdir $path/Map_reads_backto_contigs
    mkdir $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}
    $pathT/bowtie2-2.3.2/./bowtie2-build $path/DeNovo/REAssembly/${meinarrayNEW[$i]}_reas/RAContigs_perBin/unm_${meinarrayNEW[$i]}_RAContig_total.fasta $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_contig_idx 2>&1 | tee -a $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/idx_logfile.txt

sleep 10s

    $pathT/bowtie2-2.3.2/./bowtie2 -a -x $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_contig_idx -f $path/Bowtie2/Unmappedfiles/unmap_${meinarrayNEW[$i]}/interleavedPE_unmapped_${meinarrayNEW[$i]}_total.fasta -S $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.sam 2>&1 | tee -a $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/bowtie_${meinarrayNEW[$i]}_logfile.txt

sleep 10s

	samtools view -bS $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.sam > $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.bam
	samtools sort -n $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.bam -o $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.sorted.bam
	samtools index $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.sorted.bam
	samtools idxstats $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/${meinarrayNEW[$i]}_readsTOcontigs_map.sorted.bam > $path/Map_reads_backto_contigs/${meinarrayNEW[$i]}/idx_stats_${meinarrayNEW[$i]}.txt
done
#wait

