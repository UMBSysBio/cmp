# CMP

A custom-made adaptable analysis pipeline for metagenomics data, including assembly, binning, and annotation.

## Getting Started

The latest version of CMP can be obtained via git:

```
git clone https://git.ufz.de/UMBSysBio/cmp.git
```

### Prerequisites

CMP makes use of a number of software packages, which need to be available on the local machine, including:

* Prodigal
* FastQC
* FastX Tool Kit
* Trimmomatic
* Velvet
* Metaxa2 (and supporting tools)
* MetaPhlAn
* Bowtie2
* IDBA-UD
* MaxBin2
* CheckM
* bam2fastq
* samtools
* bedtools
* bamtools
* BBMap
* Diamond
* MEGAN

If the usage of other tools is desired, they can easily be replaced.

### Installing

An installation bash script is provided which installs all required tools (except for IDBA-UD, Diamond, and MEGAN) for the standard CMP (see above):

```
install.sh
```

Please check comments in the script for further instructions.

After use of the installation script and/or the manual installation of all tools please follow the following steps before calling the `main.sh` script:

```
export PATH=$PATH:/pathto/pplacer-Linux-v1.1.alpha17
export PATH=$PATH:/usr/bin/blastall
export PATH=$PATH:$pathT/ncbi-blast-2.6.0+/bin
```

Furthermore, path and names need to be specified in `main.sh`:

```
path=/path/to/your/desired/Output-Folder (e.g. path=/home/user/Exp_ID)
pathT=/path/to/the/preinstalled/Tools-Folder (e.g. pathT=/home/user/Tools)
pathSH=/path/to/the/MCB-MG-Pipeline/bin (e.g. pathSH=/home/user/MCB-MGPipeline/bin)
f=number of samples (default: 3)
NOTE: Before called the array-lists please rename your original raw read files according to following pattern:
meinarrayF=(Org_ID1_F Org_ID2_F Org_ID3_F)
meinarrayR=(Org_ID1_R Org_ID2_R Org_ID3_R)
meinarrayNEW=(ID1 ID2 ID3)
Exp=Define a specific experimental name (e.g. Exp=Experiment_ID)
```

## Usage: General Overview

CMP consists of three Bash scripts which are all called from the main script `main.sh`:
1.  `cleanup.sh` takes care of the clean-up of raw read files. Parameters for Trimmomatic can be adjusted here.
2.  `analyse_map.sh` does the pre-mapping in case of reference genomes are used. Corresponding FASTA genomes must be stored under `$path/Reference/BaseFasta`.
3.  `analyse_denovo.sh` implements assembly, binning, reassembly, and mapping of reads against contigs. Parameters for these steps can be adjusted here.

### Step 1: Preprocessing and clean-up of raw data
1.	Create some output folder for all clean-up processes
2.	Unzip the renamed original raw read files
3.	Apply FastQC on raw read files (select forward and reverse) (PreQualCheck)
4.	Clean-up and quality scan with Trimmomatic (default is PE) (CleanUp)
5.	Generate interleaved paired end files (Velvet, “shuffleSequences”) leading to Interleaved_SampleID.fastq (only PE)
6.	Create further output files with different content, resulting in CombinedUE_SampleID.fastq (only SE), Total_SampleID.fastq (PE+SE)
7.	Calculate the read length of the total cleaned file (Total_SampleID.fastq)
8.	Apply FastQC on cleaned reads of all cleaned files (PostQualCheck)

### Step 2: Pre-Taxonomical-Characterization
This step is integrated within `main.sh` and provides a first insight into the composition of the microbial community using Metaxa2.
It is a useful step to select the reference database of expected species if no other information on expected species is available.
Please refer to this [NCBI FAQ](https://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/#downloadservice) for semi-automatic download of FASTA files for the selected expected species.

### Step 3: Read Mapping
1.	It starts with Bowtie2: a) Build Bowtie2-Index depending on the desired reference database (under BaseFasta), b) Mapping with Bowtie2, creating a sam file
2.	Transform the sam file to bam with Samtools and create a bam-index to define the alignment per pre-defined species (species strand)
3.	Prepare the generated unmapped files for the de-novo analysing step
4.	Prepare the generated mapped directly for the external annotation process
5.	Calculate the read length of unmapped reads in total, as base to calculate the k-mer size for the possible de-novo assembly with ABySS or only as information.

### Step 4: De novo Assembly and Binning of unmapped (unaligned) reads
1.	Generated different output-folder for the next steps
2.	Starts IDBA-UD with the total-unmapped read file
3.	Performed a statistical analyses of the assembly with QUAST
4.	Starts binning of IDBA-UD-based contigs with Maxbin2
5.	Check of completeness and contamination rate of the bins with CheckM
6.	Reassemble the binned reads via IDBA-UD (integrated in MaxBin2)
7.	Create some folders as base for the external annotation process and copied the mapped files of Bowite2 and also the contig file of reassembly per sampleinside the respective input folders
8.	Map reads back to IDBA-UD-based contigs (experimental step)

### Step 5: External Annotation with *.sub files (diamond_daa and diamond_daa2rma)
Diamond and MEGAN6 are used here to annotate the taxonomy and potential function of mapped and unmapped files.

## Running CMP

1.  Perform Step 1 as described above.
2.	Start the `main.sh` on your command line
3.	Firstly decide between manual and automatic mode: **Manual mode:** Gives the possibility to verify that data is correctly set up (Points 4. and 5.); the user is guided through the process by a series of questions, with answering "no" to any question will exit the script.
**Automatic mode:** You have prepared all settings, folders, references, and original files (Points 4. und 5.) before and therefor you could start the full analyzing automatically without further user intervention. 
4.	Store all original raw read files (based on Illumina sequencing) inside `$path/OrgFiles` (should be generated before to avoid errors during the pipeline run) and rename the files as in the pattern under meinarrayF and meinarrayR.
5.	In best case you have one or more reference genome(s) for the first data-reduction step. In this case please download the respective genomes from the NCBI-databases (please see [NCBI FAQ](https://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/#downloadservice) for semi-automatic download of reference genomes) and save the files as FASTA files inside the folder `$path/Reference/BaseFasta` (folder and subfolder should be generated beforehand).
If it is not the case start the `main.sh` in manual mode without pre-stored reference genomes and follow the manual mode instructions (Pre-Taxonomical Characterization)!
NOTE: Only take high abundance species (> 30 matches) as reference (maybe based on metaxa-output) or take information from other methods, like 16S rRNA analysis or mcrA analysis. As reference also you can use only one reference genomes!
6.	Regardless of manual or automatic mode, at the end of analysis you will be asked if intermediate results should be deleted.


## Interpreting Output

### CMP
During the analysis many intermediated results are generated which can be used to check for the correctness of pipeline steps and for further analysis.
Inside the `$path/` folder, the following sub-folders will be created:

*  `$path/OrgFiles` will be created by yourself or during the first part of the analysis and contains all original raw read files renamed according to the pattern "Org_IDx_F.fastq(.gz)" and "Org_IDx_R.fastq(.gz)".
*  `$path/Reference` will be created by yourself or during the first part of the analysis and contains the folder `$path/Reference/BaseFasta`. Here your reference genome file(s) in FASTA format should be stored. Later, some log files are generated here as the base for the Index-building by Bowtie2.
*  `$path/PreQualCheck` contains the output files generated by FastQC on the original raw read files.
*  `$path/CleanUp` contains all cleaned and trimmed files. These files are the base for all further steps.
*  `$path/CleanUp/Clean_IDx` contains the Trimmomatic output. (1P|2P=forward paired and reverse paired, 1U|2U=forward unpaired and reverse unpaired)
* Additionaly, `$path/CleanUp/` contains the following files:
	- `CombinedUP_IDx.fastq` (only unpaired clean reads)
	- `InterlavedPE_IDx.fastq/fasta` (only paired end clean reads)
	- `Total_IDx.fastq` (paired and unpaired clean reads)
	- `read Dist_clean_IDx.txt` (read length distribution of cleaned reads per ID)
	- `trim_logfile`
*  `$path/PostQualCheck` contains the FastQC results obtained for the cleaned reads.
*  `$path/PreTax` contains Metaxa2 output files, which can be used to select species for the reference database. (Important: ttt level 7 output)
*  `$path/Bowtie2` contains output of the pre-mapping step based on the user-defined reference database
	- `$path/Bowtie2/IDX`: all Bowtie2 indexes
	- `$path/Bowtie2/IDx`: Bowtie2 output of bam and sam files and logfile that contains the mapping statistics + idx_stats per sample that contains a tab of the genome/strain that mapped (Name|length|mapped|unmapped)
	- `$path/Bowtie2/Mappingfiles`: Contains only the mapped reads per ID in different formats.
	- `$path/Bowtie2/Unmappedfiles`: Contains only the unmapped reads per ID in different formats and read length distribution files selected for all unmapped reads or only paired end (interleaved) reads; for next steps only the interleaved file will be important
	- `$path/Bowtie2/idx_inspect_logfile.txt`: Contains the content of bowtie-index.
	- `$path/Bowtie2/idx_logfile.txt`: Contains log information.
*  `$path/DeNovo`: This part is only for unmapped reads. The main folder combines all output folders of the de-novo analysis step:
	- `$path/DeNovo/Assembly`: Contains only the assembly output and statistics of the assembly.
	    - `$path/DeNovo/Assembly/IDBA`: IDBA_UD output per sample, contig.fa is the final file.
	    - `$path/DeNovo/Assembly/Quast`: Statistical analysis of the assembly, summarized in `report.pdf`.
		- `$path/DeNovo/Assembly/IDx`: MaxBin2 output per sample.
		- `$path/DeNovo/Assembly/CheckM`: Quality check of bins per sample (if it is possible), all important information are saved in `logfile_checkm_IDx` per sample.
	- `$path/DeNovo/Bin_Contig`: Contains only the binning output and statistics of assembly.
    - `$path/DeNovo/REAssembly`: Contains only the reassembly outputs and post statistics of reassembly with IDBA-UD.
		- `$path/DeNovo/Assembly/IDx_reas`: Reassembly results under `IDx_idba_*` and final file (combining all contigs per bin in one file under `RAContigs_perBin`.
		- `$path/DeNovo/Assembly/PostRA_QUAST`: Statistical analysis of the assembly, summary in `report.pdf`.
*  `$path/Annotation` contains all files which are the base input for external annotation with Diamond and MEGAN6, per sample (grouped mapped/unmapped).

### External annotation with Diamond and MEGAN6
To finalize our analysis process, the annotation is performed externally. This allows for switching to a more powerfull machine as Diamond needs a lot of computational power to generate an NCBI-database based mapping file per sample.
In case no suitable computational facilities are availble, consider using computationally less demanding tools such as [Prokka](https://github.com/tseemann/prokka) for functional annotation.

The first step for annotation is to generate an `nr.dmnd` file based on `nr.gz`, downloaded from NCBI, with Diamond. This step must be performed only at the first time:

```
wget ftp://ftp.ncbi.nih.gov/blast/db/FASTA/nr.gz
diamond makedb –in /path/to/nr.gz –db /path/to/store/nr
```

After generation of the `nr.dmnd` file, the blastx run of Diamond can started:

```
diamond blastx –q /path-to-input/all_mapped_IDx_total.fasta –d /path-to/nr –a /path-to-outputfolder/all_mapped_IDx_total.daa
diamond blastx –q /path-to-input/unm_IDx_RAContig_total.fasta –d /path-to/nr –a /path-to-outputfolder/unm_IDx_RAContig_total.daa
```
NOTE: IDx must be renamed with your individual sample-IDs (the input is found under `$path/Annotation/IDx/mapped_annotation/input` and `$path/Annotation/IDx/unmapped_annotation/input`.

Now the `*.daa` files must be transformed into `*.rma` files using MEGAN6:

```
daa2rma –i /path-to-outputfolder/unm_IDx_RAContig_total.daa –o /path-to-outputfolder/unm_IDx_RAContig_total.rma –a2t /data/umbsysbio/Dani/DB/prot_acc2tax-May2017.abin -a2eggnog /data/umbsysbio/Dani/DB/acc2eggnog-Oct2016X.abin -fun EGGNOG
daa2rma –i /path-to-outputfolder/all_mapped_IDx_total.daa –lg –o /path-to-outputfolder/all_mapped_IDx_total.rma –a2t /data/umbsysbio/Dani/DB/prot_acc2tax-May2017.abin -a2eggnog /data/umbsysbio/Dani/DB/acc2eggnog-Oct2016X.abin -fun EGGNOG
```
`-lg` means long reads, because the unmapped reads are contigs,	`-pof` could be also an additional setting for paired end reads in one file; `-fwa` indicates first word is accession

Finally, `*.rma` files can be loaded in MEGAN6 ("Open"). Look at the MEGAN-Manual to find out, which possibilities for analysis are availbale. 

### Combining taxonomic and functional information

To combine taxonomic with functional information for reads, the R script `AnnotationCombine.r` can be used:
1.  Open the `*.rma` file in Megan6
2.  In the taxonomic tree: uncollapse the tree and select the relevant subtrees (including all sub-nodes!)
3.  Export as CSV, selecting the option "read-id-taxonomic-path-including-percentage" (export what=CSV format=readName_to_taxonPathPercent separator=tab counts=summarized) 
4.  In the functional tree, unncollapse tree and select all leafs only
5.  Export as CSV, selecting the option "read-id-functional-path" (export what=CSV format=readName_to_eggnogPath separator=tab counts=summarized)
6.  Make sure that in both cases, the number of lines in exported CSV files matches values reported in the Megan tree visualizations! (The script assumes that for each read, there is one unique annotation for taxonomy and/or function) 
7.  Adjust filenames in `AnnotationCombine.R`, and select appropriate levels to analyze (close to the end of the script)
8.  Run `AnnotationCombine.R` (Rstudio recommended, where subsequently other levels can be analyzed as well) 

## Adapting CMP
If other than the default tools should be used for the analysis, they can be replaced by editing the corresponding script file, for example choosing another assembler than IDBA-UD by editing `analyze_denovo.sh` and changing the calls to IDBA-UD to the new tool.
Note however, that care must be taken that the output format of the new tool is compatible with the default tool, otherwise more elaborate modifications might become necessary.

## Author

* **Daniela Becker** (all except AnnotationCombine.r), UFZ - Helmholtz Centre for Environmental Research, Leipzig, Germany
* **Florian Centler** (AnnotationCombine.r), UFZ - Helmholtz Centre for Environmental Research, Leipzig, Germany

Contact: daniela.taraba@ufz.de

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details
