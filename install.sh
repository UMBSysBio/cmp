#!/bin/bash

#########################################################
##########Section of path##########
#########################################################
##define the desired folder to install all important tools to use the MG-analyzing pipeline
##Attention please save all tools with a folder that contains executable files in the same tool folder
###E.g: pathB=/home/user
pathB=/media/admin/DATA/Dani_Daten


#########################################################
##########REQUIREMENTS##########
#########################################################

### 64bit Linux version
### 12G physical memory
### make
### sudo privileges
### [Boost](http://www.boost.org/)
### [Open MPI](http://www.open-mpi.org)
### [sparsehash](https://code.google.com/p/sparsehash/)
### [SQLite](http://www.sqlite.org/)
### gcc/g++ (>=4.7)
### (c++ 11 or higher)
### perl5
### python2
### prodigal (2.60 or >=2.6.1) (executable must be named prodigal)
### pplacer (>=1.1) (add to you system path!!!!)

###############################################################################################################################################################################################

sudo apt-get update 
sudo apt-get install -y pkg-config libfreetype6-dev libpng-dev python-matplotlib
sudo apt-get install python
sudo apt-get install python3
sudo apt-get install perl
sudo apt-get install build-essential
sudo apt-get install python-pip
sudo apt-get install python3-pip
sudo apt-get install unzip
sudo apt-get install git
sudo apt-get install build-essential
sudo apt-get install tar tar-doc
sudo apt-get install libtbb-dev
sudo apt-get install autoconf
sudo apt-get install automake
sudo apt-get install build-essential g++ python-dev autotools-dev libicu-dev build-essential libbz2-dev libboost-all-dev
sudo apt-get install sparsehash
sudo apt-get install cmake cmake-qt-gui
sudo apt-get install samtools
sudo apt-get install libncurses5-dev
sudo apt-get install build-essential
sudo apt-get install liblzma-doc
sudo apt-get install liblzma5

#########################################################
#Don't touche the source code#
#########################################################

#########################################################
##########INSTALL##########
#########################################################

###automatically generation of the main folder named Tools und define the pathT path!
mkdir $pathB/Tools/
pathT=$pathB/Tools

echo "-----------------INSTALL Prodigal 2.6.3-----------------"
###download the prodigal package
wget -O $pathT/v2.6.3.tar.gz https://github.com/hyattpd/Prodigal/archive/v2.6.3.tar.gz
cd $pathT/
###unzip the source package
tar -xzf $pathT/v2.6.3.tar.gz
rm $pathT/v2.6.3.tar.gz
cd $pathT/Prodigal-2.6.3
sudo make install
###to /usr/local/bin
make
###rename the executable prodigal file in "prodigal"
cd $pathT/

echo "-----------------INSTALL FastQC-----------------"
mkdir $pathT/FastQC
sudo apt-get install fastqc
###the executable file must be executable, no compilation is necessary
cp /usr/bin/fastqc $pathT/FastQC/
wget –O $pathT/FastQC.zip https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip
unzip $pathT/FastQC
cd $pathT/


echo "-----------------INSTALL FastX Tool Kit-----------------"
sudo apt install fastx-toolkit
cd $pathT/

echo "-----------------INSTALL Trimmomatic-----------------"
###only the source must be downloaded
wget -O $pathT/Trimmomatic-0.36.zip http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.36.zip
cd $pathT/
unzip Trimmomatic-0.36.zip
rm Trimmomatic-0.36.zip
cd $pathT/

echo "-----------------INSTALL shuffleSequences-----------------"
###D.R. Zerbino and E. Birney. 2008. Velvet: algorithms for de novo short read assembly using de Bruijn graphs. Genome Research, 18:821-829
wget -O $pathT/velvet_1.2.10.tgz https://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.10.tgz
cd $pathT/
tar -xzf velvet_1.2.10.tgz
cd $pathT/velvet_1.2.10/
make
rm ../velvet_1.2.10.tgz
cd $pathT/

echo "-----------------INSTALL Metaxa2 and required tools-----------------"
###Bengtsson-Palme J, Hartmann M, Eriksson KM, Pal C, Thorell K, Larsson DGJ, Nilsson RH: Metaxa2: Improved Identification and Taxonomic Classification of Small and Large Subunit rRNA in Metagenomic Data. Molecular Ecology Resources (2015). doi: 10.1111/1755-0998.12399
wget -O $pathT/Metaxa2_2.1.3.tar.gz http://microbiology.se/sw/Metaxa2_2.1.3.tar.gz
wget -O $pathT/hmmer-3.1b2-linux-intel-x86_64.tar.gz http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz
###manuel download could be necessary
wget -O $pathT/ncbi-blast-2.6.0+-x64-linux.tar.gz ftp://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/ncbi-blast-2.6.0+-x64-linux.tar.gz
wget -O $pathT/mafft-7.310-with-extensions-src.tgz http://mafft.cbrc.jp/alignment/software/mafft-7.310-with-extensions-src.tgz
cd $pathT/
tar -xzf $pathT/hmmer-3.1b2-linux-intel-x86_64.tar.gz
cd $pathT/hmmer-3.1b2-linux-intel-x86_64
./configure
make
sudo make install
cd $pathT/
tar zxvpf $pathT/ncbi-blast-2.6.0+-x64-linux.tar.gz
sudo apt install blast2
tar -xzf $pathT/mafft-7.310-with-extensions-src.tgz
cd $pathT/mafft-7.310-with-extensions
cd core
sudo make install
make
cd $pathT/
tar -xzf $pathT/Metaxa2_2.1.3.tar.gz
cd $pathT/Metaxa2_2.1.3
./install_metaxa2
cd $pathT/
rm $pathT/hmmer-3.1b2-linux-intel-x86_64.tar.gz -C $pathT/
###rm $pathT/ncbi-blast-2.6.0+-x64-linux.tar.gz
rm $pathT/Metaxa2_2.1.3.tar.gz
rm $pathT/mafft-7.310-with-extensions-src.tgz
cd $pathT/

echo "-----------------INSTALL MetaPhlAn-----------------"
###"Metagenomic microbial community profiling using unique clade-specific marker genes", Nicola Segata, Levi Waldron, Annalisa Ballarini, Vagheesh Narasimhan, Olivier Jousson, Curtis Huttenhower. Nature Methods, 8, 811–814, 2012
wget -O $pathT/default.tar.bz2 https://bitbucket.org/nsegata/metaphlan/get/default.tar.bz2
cd $pathT/
tar -xjvf $pathT/default.tar.bz2
mv *-metaphlan-* metaphlan
rm $pathT/default.tar.bz2
cd $pathT/metaphlan
pip install numpy
pip install matplotlib
sudo apt-get install python-scipy
pip install scipy
cd $pathT/

echo "-----------------INSTALL Bowtie2-----------------"
wget -O $pathT/bowtie2-2.3.2-source.zip https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.3.2/bowtie2-2.3.2-source.zip
cd $pathT/
unzip bowtie2-2.3.2-source.zip
rm $pathT/bowtie2-2.3.2-source.zip
cd $pathT/bowtie2-2.3.2
make
cd $pathT/
sudo apt-get install samtools
cd $pathT/

echo "-----------------INSTALL ABySS-----------------"
###ABySS
###A parallel assembler for short read sequence data. Simpson JT, Wong K, Jackman SD, Schein JE, Jones SJ, Birol I. Genome Research, 2009-June. (Genome Research, PubMed)
###Install ABySS on /usr/local
sudo apt-get install abyss
cd $pathT/
sudo apt-get install libboost-all-dev
wget -O $pathT/boost_1_56_0.tar.bz2 http://downloads.sourceforge.net/project/boost/boost/1.56.0/boost_1_56_0.tar.bz2
tar jxf boost_1_56_0.tar.bz2
rm $pathT/boost_1_56_0.tar.bz2
cd $pathT/
git clone https://github.com/sparsehash/sparsehash.git $pathT/
cd $pathT/sparsehash
./configure
make
sudo make install
cd $pathT/
git clone https://github.com/bcgsc/abyss.git $pathT/
cd $pathT/abyss
./autogen.sh
$pathT/abyss/./configure
###only successful, if all requirements are available and the configuration ouput shows no errors or fails at the end of configuration!
make
sudo make install
$pathT/abyss/./configure CC=gcc-4.6 CXX=g++-4.6
$pathT/abyss/./configure --with-boost=/usr/local/include
$pathT/abyss/./configure --with-mpi=/usr/lib/openmpi
$pathT/abyss/./configure CPPFLAGS=-I/usr/local/include
$pathT/abyss/./configure --with-sqlite=/opt/sqlite3
$pathT/abyss/./configure --enable-maxk=320
make AM_CXXFLAGS=-Wall
cd $pathT/

echo "-----------------INSTALL MaxBin2-----------------"
###Wu YW, Tang YH, Tringe SG, Simmons BA, and Singer SW, "MaxBin: an automated binning method to recover individual genomes from metagenomes using an expectation-maximization algorithm", Microbiome, 2:26, 2014.
###Wu YW, Simmons BA, and Singer SW, "MaxBin 2.0: an automated binning algorithm to recover genomes from multiple metagenomic datasets", Bioinformatics, 32(4): 605-607, 2016.
wget -O $pathT/MaxBin-2.2.4.tar.gz https://sourceforge.net/projects/maxbin2/files/MaxBin-2.2.4.tar.gz
cd $pathT/
tar -xzf MaxBin-2.2.4.tar.gz
rm $pathT/MaxBin-2.2.4.tar.gz
cd $pathT/MaxBin-2.2.4
./autobuild_auxiliary
cd $pathT/MaxBin-2.2.4/src
make
echo "IDBA_UD installation about the idba_ud folder under auxiliary --> transform k-mer size manual --> show Install-Structions"
cd $pathT/

echo "-----------------INSTALL Stat. Tools-----------------"
###QUAST
###Alexey Gurevich, Vladislav Saveliev, Nikolay Vyahhi and Glenn Tesler,QUAST: quality assessment tool for genome assemblies,Bioinformatics (2013) 29 (8): 1072-1075. doi: 10.1093/bioinformatics/btt086, First published online: February 19, 2013
wget -O $pathT/quast-4.5.tar.gz https://downloads.sourceforge.net/project/quast/quast-4.5.tar.gz
cd $pathT/
tar -xzf $pathT/quast-4.5.tar.gz
cd $pathT/quast-4.5
./setup.py install_full
rm $pathT/quast-4.5.tar.gz
cd $pathT/

###CheckM
###Parks DH, Imelfort M, Skennerton CT, Hugenholtz P, Tyson GW. 2014. Assessing the quality of microbial genomes recovered from isolates, single cells, and metagenomes. Genome Research, 25: 1043-1055.
sudo pip install numpy
wget -O $pathT/pplacer-Linux-v1.1-alpha17.zip https://github.com/matsen/pplacer/releases/download/v1.1.alpha17/pplacer-Linux-v1.1.alpha17.zip
cd  $pathT/
unzip pplacer-Linux-v1.1-alpha17.zip
cd $pathT/pplacer-Linux-v1.1.alpha17
export PATH="$pathT/pplacer-Linux-v1.1.alpha17:$PATH"
###NOTE: During the CheckM installation you must define a path for updates, e.g. $pathT/checkm/
sudo pip install checkm-genome
sudo checkm data update
cd $pathT/

echo "-----------------INSTALL further Tools-----------------"
###bam2fastq
cd $pathT/
git clone --recursive https://github.com/grenaud/BCL2BAM2FASTQ.git $pathT/
cd $pathT/
cd $pathT/BCL2BAM2FASTQ/
cd bamtools/
mkdir build/
cd $pathT/BCL2BAM2FASTQ/bamtools/build/
cmake ..
make
cd ../..

###samtools
###download samtools
git clone git://github.com/samtools/samtools.git $pathT/
###a samtools folder is generated
###download htslib inside this samtools folder
###rename htslib_1.6 in htslib!
wget -O $pathT/samtools/htslib.tar.gz https://github.com/samtools/htslib/archive/1.6.tar.gz
tar -xzf $pathT/samtools/htslib.tar.gz
###install some necessary packages
###see above
# # # sudo apt-get install libncurses5-dev
# # # sudo apt-get install build-essential
# # # sudo apt-get install liblzma-doc
# # # sudo apt-get install liblzma5
###navigate inside the directory samtools/htslib:
cd $pathT/samtools/htslib
autoheader
autoconf
./configure --disable-lzma
make
sudo make install
###navigate inside the directory samtools:
cd $pathT/samtools
autoheader
autoconf -Wno-syntax
./configure --disable-lzma
make
sudo make install


###bedtools
sudo apt-get install bedtools

####bamtools
sudo apt install bamtools

###bbmap
wget -O $pathT/BBMap_37.57.tar.gz https://sourceforge.net/projects/bbmap/files/latest/download/BBMap_37.57.tar.gz
cd $pathT/
tar -xzf BBMap_37.57.tar.gz


echo -e "\033[43mNOTE: Diamond and MEGAN should be only used on a cluster server system\033[0m."
###Diamond and MEGAN should be only used on a cluster server system
###The reason is the high memory usage
###In best case you should use bowtie2 also on a cluster systems



















